
png(filename="analysis/revigo/enriched/bp.png",res=300,units="in",height=8,width=8)
# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800


# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );


# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0006825","copper ion transport", 0.027,-4.435, 1.509, 4.128,-3.9057,0.796,0.000),
c("GO:0006979","response to oxidative stress", 0.539, 5.763,-2.914, 5.428,-3.6391,0.599,0.000),
c("GO:0015979","photosynthesis", 0.343,-0.933, 4.340, 5.232,-11.6592,0.782,0.000),
c("GO:0030244","cellulose biosynthetic process", 0.016,-2.446,-6.318, 3.899,-1.8247,0.670,0.040),
c("GO:0019684","photosynthesis, light reaction", 0.085, 3.803, 5.005, 4.627,-5.6952,0.571,0.045),
c("GO:0006351","transcription, DNA-templated", 9.014,-4.264,-3.820, 6.652,-2.7224,0.580,0.261),
c("GO:0006012","galactose metabolic process", 0.110,-0.807,-8.091, 4.740,-1.4946,0.709,0.412),
c("GO:0009607","response to biotic stimulus", 0.465, 6.353,-1.833, 5.364,-2.8280,0.624,0.418),
c("GO:0000160","phosphorelay signal transduction system", 2.126, 6.166,-3.847, 6.024,-2.6405,0.544,0.488),
c("GO:0006952","defense response", 0.569, 6.668,-3.000, 5.452,-2.1447,0.598,0.588));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below

p1 <- ggplot( data = one.data );
p1 <- p1 + geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.6) ) + scale_size_area();
p1 <- p1 + scale_colour_gradientn( colours = c("blue", "green", "yellow", "red"), limits = c( min(one.data$log10_p_value), 0) );
p1 <- p1 + geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + scale_size_area();
p1 <- p1 + scale_size( range=c(5, 30)) + theme_bw(); # + scale_fill_gradientn(colours = heat_hcl(7), limits = c(-300, 0) );
ex <- one.data [ one.data$dispensability < 0.15, ]; 
p1 <- p1 + geom_text( data = ex, aes(plot_X, plot_Y, label = description), colour = I(alpha("black", 0.85)), size = 3 );
p1 <- p1 + ggtitle("Biological Process") + theme(axis.title = element_blank());
p1 <- p1 + theme(legend.key = element_blank()) ;
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
p1 <- p1 + xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10);
p1 <- p1 + ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10);



# --------------------------------------------------------------------------
# Output the plot to screen

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

ggsave("analysis/revigo/enriched/bp.png", p1);

dev.off()
