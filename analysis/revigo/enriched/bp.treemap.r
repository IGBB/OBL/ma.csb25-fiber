
png(filename="analysis/revigo/enriched/bp.treemap.png",res=300,units="in",height=4,width=8)
# A treemap R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

# author: Anton Kratz <anton.kratz@gmail.com>, RIKEN Omics Science Center, Functional Genomics Technology Team, Japan
# created: Fri, Nov 02, 2012  7:25:52 PM
# last change: Fri, Nov 09, 2012  3:20:01 PM

# -----------------------------------------------------------------------------
# If you don't have the treemap package installed, uncomment the following line:
# install.packages( "treemap" );
library(treemap) 								# treemap package by Martijn Tennekes

# Set the working directory if necessary
# setwd("C:/Users/username/workingdir");

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","freqInDbPercent","abslog10pvalue","uniqueness","dispensability","representative");
revigo.data <- rbind(c("GO:0006825","copper ion transport",0.027,3.9057,0.796,0.000,"copper ion transport"),
c("GO:0006979","response to oxidative stress",0.539,3.6391,0.599,0.000,"response to oxidative stress"),
c("GO:0009607","response to biotic stimulus",0.465,2.8280,0.624,0.418,"response to oxidative stress"),
c("GO:0000160","phosphorelay signal transduction system",2.126,2.6405,0.544,0.488,"response to oxidative stress"),
c("GO:0006952","defense response",0.569,2.1447,0.598,0.588,"response to oxidative stress"),
c("GO:0015979","photosynthesis",0.343,11.6592,0.782,0.000,"photosynthesis"),
c("GO:0030244","cellulose biosynthetic process",0.016,1.8247,0.670,0.040,"cellulose biosynthesis"),
c("GO:0006351","transcription, DNA-templated",9.014,2.7224,0.580,0.261,"cellulose biosynthesis"),
c("GO:0006012","galactose metabolic process",0.110,1.4946,0.709,0.412,"cellulose biosynthesis"),
c("GO:0019684","photosynthesis, light reaction",0.085,5.6952,0.571,0.045,"photosynthesis, light reaction"));

stuff <- data.frame(revigo.data);
names(stuff) <- revigo.names;

stuff$abslog10pvalue <- as.numeric( as.character(stuff$abslog10pvalue) );
stuff$freqInDbPercent <- as.numeric( as.character(stuff$freqInDbPercent) );
stuff$uniqueness <- as.numeric( as.character(stuff$uniqueness) );
stuff$dispensability <- as.numeric( as.character(stuff$dispensability) );

# check the tmPlot command documentation for all possible parameters - there are a lot more
treemap(
	stuff,
	index = c("representative","description"),
	vSize = "abslog10pvalue",
	type = "categorical",
	vColor = "representative",
	title = "REVIGO Gene Ontology treemap",
	inflate.labels = FALSE,      # set this to TRUE for space-filling group labels - good for posters
	lowerbound.cex.labels = 0,   # try to draw as many labels as possible (still, some small squares may not get a label)
	bg.labels = "#CCCCCCAA",     # define background color of group labels
												       # "#CCCCCC00" is fully transparent, "#CCCCCCAA" is semi-transparent grey, NA is opaque
	position.legend = "none"
)


dev.off()
