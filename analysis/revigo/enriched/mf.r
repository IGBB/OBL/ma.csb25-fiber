
png(filename="analysis/revigo/enriched/mf.png",res=300,units="in",height=8,width=8)
# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800


# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );


# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0000976","transcription regulatory region sequence-specific DNA binding", 0.031, 1.047, 3.432, 4.162,-4.9809,0.836,0.000),
c("GO:0004601","peroxidase activity", 0.358, 6.433, 0.483, 5.229,-5.3990,0.796,0.000),
c("GO:0005200","structural constituent of cytoskeleton", 0.047, 4.143, 6.033, 4.350,-1.4946,0.852,0.000),
c("GO:0005215","transporter activity",12.041, 1.879,-2.809, 6.756,-4.8473,0.869,0.000),
c("GO:0016987","sigma factor activity", 0.443,-2.814,-0.312, 5.321,-1.6831,0.853,0.000),
c("GO:0045156","electron transporter, transferring electrons within the cyclic electron transport pathway of photosynthesis activity", 0.036,-7.295,-0.779, 4.234,-4.2072,0.852,0.000),
c("GO:0016760","cellulose synthase (UDP-forming) activity", 0.012,-2.924,-5.931, 3.744,-2.6837,0.819,0.015),
c("GO:0004197","cysteine-type endopeptidase activity", 0.102,-4.476, 5.115, 4.682,-3.2109,0.814,0.018),
c("GO:2001070","starch binding", 0.006, 1.577,-6.915, 3.487,-1.4207,0.838,0.029),
c("GO:0030151","molybdenum ion binding", 0.271, 5.560,-4.451, 5.108,-1.9385,0.835,0.037),
c("GO:0051536","iron-sulfur cluster binding", 2.612,-0.323, 7.351, 6.092,-1.4207,0.836,0.055),
c("GO:0000155","phosphorelay sensor kinase activity", 1.068,-4.720,-4.832, 5.704,-1.4785,0.816,0.173),
c("GO:0019829","cation-transporting ATPase activity", 0.770,-5.693, 3.834, 5.562,-2.0366,0.813,0.203),
c("GO:0016651","oxidoreductase activity, acting on NAD(P)H", 1.351, 6.298, 1.503, 5.805,-1.9385,0.796,0.330));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below

p1 <- ggplot( data = one.data );
p1 <- p1 + geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.6) ) + scale_size_area();
p1 <- p1 + scale_colour_gradientn( colours = c("blue", "green", "yellow", "red"), limits = c( min(one.data$log10_p_value), 0) );
p1 <- p1 + geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + scale_size_area();
p1 <- p1 + scale_size( range=c(5, 30)) + theme_bw(); # + scale_fill_gradientn(colours = heat_hcl(7), limits = c(-300, 0) );
ex <- one.data [ one.data$dispensability < 0.15, ]; 
p1 <- p1 + geom_text( data = ex, aes(plot_X, plot_Y, label = description), colour = I(alpha("black", 0.85)), size = 3 );
p1 <- p1 + ggtitle("Molecular Function") + theme(axis.title = element_blank());
p1 <- p1 + theme(legend.key = element_blank()) ;
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
p1 <- p1 + xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10);
p1 <- p1 + ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10);



# --------------------------------------------------------------------------
# Output the plot to screen

ggsave("analysis/revigo/enriched/mf.png", p1);

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

# ggsave("C:/Users/path_to_your_file/revigo-plot.pdf");

dev.off()
