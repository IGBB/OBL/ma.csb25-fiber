
png(filename="analysis/revigo/enriched/mf.treemap.png",res=300,units="in",height=4,width=8)
# A treemap R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

# author: Anton Kratz <anton.kratz@gmail.com>, RIKEN Omics Science Center, Functional Genomics Technology Team, Japan
# created: Fri, Nov 02, 2012  7:25:52 PM
# last change: Fri, Nov 09, 2012  3:20:01 PM

# -----------------------------------------------------------------------------
# If you don't have the treemap package installed, uncomment the following line:
# install.packages( "treemap" );
library(treemap) 								# treemap package by Martijn Tennekes

# Set the working directory if necessary
# setwd("C:/Users/username/workingdir");

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","freqInDbPercent","abslog10pvalue","uniqueness","dispensability","representative");
revigo.data <- rbind(c("GO:0000976","transcription regulatory region sequence-specific DNA binding",0.031,4.9809,0.836,0.000,"transcription regulatory region sequence-specific DNA binding"),
c("GO:0004601","peroxidase activity",0.358,5.3990,0.796,0.000,"peroxidase activity"),
c("GO:0016651","oxidoreductase activity, acting on NAD(P)H",1.351,1.9385,0.796,0.330,"peroxidase activity"),
c("GO:0005200","structural constituent of cytoskeleton",0.047,1.4946,0.852,0.000,"structural constituent of cytoskeleton"),
c("GO:0005215","transporter activity",12.041,4.8473,0.869,0.000,"transporter activity"),
c("GO:0016987","sigma factor activity",0.443,1.6831,0.853,0.000,"sigma factor activity"),
c("GO:0045156","electron transporter, transferring electrons within the cyclic electron transport pathway of photosynthesis activity",0.036,4.2072,0.852,0.000,"electron transporter, transferring electrons within the cyclic electron transport pathway of photosynthesis activity"),
c("GO:0016760","cellulose synthase (UDP-forming) activity",0.012,2.6837,0.819,0.015,"cellulose synthase (UDP-forming) activity"),
c("GO:0000155","phosphorelay sensor kinase activity",1.068,1.4785,0.816,0.173,"cellulose synthase (UDP-forming) activity"),
c("GO:0004197","cysteine-type endopeptidase activity",0.102,3.2109,0.814,0.018,"cysteine-type endopeptidase activity"),
c("GO:0019829","cation-transporting ATPase activity",0.770,2.0366,0.813,0.203,"cysteine-type endopeptidase activity"),
c("GO:2001070","starch binding",0.006,1.4207,0.838,0.029,"starch binding"),
c("GO:0030151","molybdenum ion binding",0.271,1.9385,0.835,0.037,"molybdenum ion binding"),
c("GO:0051536","iron-sulfur cluster binding",2.612,1.4207,0.836,0.055,"iron-sulfur cluster binding"));

stuff <- data.frame(revigo.data);
names(stuff) <- revigo.names;

stuff$abslog10pvalue <- as.numeric( as.character(stuff$abslog10pvalue) );
stuff$freqInDbPercent <- as.numeric( as.character(stuff$freqInDbPercent) );
stuff$uniqueness <- as.numeric( as.character(stuff$uniqueness) );
stuff$dispensability <- as.numeric( as.character(stuff$dispensability) );

treemap(
	stuff,
	index = c("representative","description"),
	vSize = "abslog10pvalue",
	type = "categorical",
	vColor = "representative",
	title = "REVIGO Gene Ontology treemap",
	inflate.labels = FALSE,      # set this to TRUE for space-filling group labels - good for posters
	lowerbound.cex.labels = 0,   # try to draw as many labels as possible (still, some small squares may not get a label)
	bg.labels = "#CCCCCCAA",     # define background color of group labels
												       # "#CCCCCC00" is fully transparent, "#CCCCCCAA" is semi-transparent grey, NA is opaque
	position.legend = "none"
)
dev.off()
