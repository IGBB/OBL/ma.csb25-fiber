Left reads:
          Input     :  32372435
           Mapped   :  32118826 (99.2% of input)
            of these:  15184800 (47.3%) have multiple alignments (210053 have >20)
Right reads:
          Input     :  32372435
           Mapped   :  32168499 (99.4% of input)
            of these:  15199727 (47.3%) have multiple alignments (210075 have >20)
99.3% overall read mapping rate.

Aligned pairs:  31937099
     of these:  15107668 (47.3%) have multiple alignments
                 4148268 (13.0%) are discordant alignments
85.8% concordant pair alignment rate.
